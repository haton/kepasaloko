/*global require*/
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var bodyParser = require('body-parser');
var exphbs = require('express-handlebars');
var mongoose = require('mongoose');
var app = express();
var debug = require('debug')('xat:server');
var http = require('http').Server(app);
var io = require('socket.io')(http);
var sharedsession = require("socket.io-express-session");

var routes = require('./routes/index');
var route_users = require('./routes/users');
var route_missatges = require('./routes/missatges');
var route_polls = require('./routes/polls');

var Missatge = require('./models/missatge');
var User = require('./models/user');
var Poll = require('./models/poll').poll;
var Choice = require('./models/poll').choice;
var Vote = require('./models/poll').vote;

var usersOnline = [];


// mongodb
mongoose.connect('mongodb://localhost:27017/kepasaloko'); // conección a mongo

// view engine setup
handlebars = exphbs.create({
	defaultLayout: 'main',
	//helpers      : helpers,
	extname: '.html', //set extension to .html so handlebars knows what to look for

	// Uses multiple partials dirs, templates in "shared/templates/" are shared
	// with the client-side of the app (see below).
	partialsDir: [
		//'views/shared/',
		'views/partials/'
	]
});

app.engine('html', handlebars.engine);
app.set('view engine', 'html');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

// middleware
app.use(logger('dev'));
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(cookieParser());
app.use(express.static('public'));
app.use('/static', express.static('public'));

// session
sess = session({
	name: 'session',
	secret: 'keyboard cat',
	resave: true,
	saveUninitialized: true
});
app.use(sess);

// routers
app.use('/', routes);
app.use('/api', route_users);
app.use('/api', route_missatges);
app.use('/api', route_polls);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
	app.use(function(err, req, res, next) {
		res.status(err.status || 500);
		res.render('error', {
			message: err.message,
			layout: 'error',
			error: err
		});
	});
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
	res.status(err.status || 500);
	res.render('error', {
		message: err.message,
		layout: 'error',
		error: {}
	});
});


io.use(sharedsession(sess, {
	autoSave: true
}));

// actualitzar hora
function actualitzarHora(socket) {
	User.findById(socket.handshake.session.uid, function(err, user) {
		if (!err && user !== null) {
			socket.handshake.session.darrera_connexio = Date.now();
			user.darrera_connexio = Date.now();
			user.save(function(err) {});
		}
	});
}

// socket.io
io.on('connection', function(socket) {

  actualitzarHora(socket);
  
	// si no hi és, afegim l'usuari a la llista de connectats
	if (usersOnline.indexOf(socket.handshake.session.uid) == -1) {
		usersOnline.push(socket.handshake.session.uid);
	}

	// emitim els usuaris connectats
  //esperam un segon per a que el mateix usuari es pugui veue
  io.emit('users online', {
		'users_online': usersOnline
	});


	socket.on('chat message', function(msg) {
		actualitzarHora(this);
		var missatge = new Missatge();
		missatge.contingut = msg;

		//comprovam que tengui la sessío iniciada
		if (typeof socket.handshake.session.uid !== 'undefined') {
			if (missatge.contingut !== '') {
				missatge.emisor = socket.handshake.session.uid;
				missatge.save(function(err) {});
				missatge.nom = socket.handshake.session.nom;
				io.emit('chat message', {
					'missatge': missatge,
					'nom': socket.handshake.session.nom,
					'numusers': io.engine.clientsCount,
					'darrera_connexio': Date.now(),
					'users_online': usersOnline
				});
			}
		}
	});

	socket.on('poll', function(p) {
		var choices = [];
		for (var c in p.choices) {
			choices.push(new Choice({
				text: p.choices[c]
			}));
		}
		var poll = new Poll();
		poll.title = p.title;
		poll.choices = choices;
		poll.user = socket.handshake.session.uid;
		poll.save(function(err, doc) {
			if (err || !doc) {
				throw 'Error';
			} else {
				io.emit('poll', doc);
			}
		});
	});

	socket.on('vote', function(p) {
		var vote = new Vote();
		vote.uid = socket.handshake.session.uid;
    var comprovarVot = function(poll, vote, callback){
      var contador = 0;
      var exists = false;
      for (var c in poll.choices) {
        if(!isNaN(c)){
          for (var v in poll.choices[c].votes){
              if(!isNaN(v)){
                contador++;
                if (JSON.stringify(poll.choices[c].votes[v].uid) === JSON.stringify(vote.uid)){
                  exists = true;
                  console.log(contador);
                  break;
                }
              }
          }
        }
      }
      if (exists){
        callback(null);
      }
      else {
        callback(poll);
      }
    };
    var afegirVot = function(poll, vote, p, callback){
      for (var c in poll.choices) {
        if(!isNaN(c)){
          if (poll.choices[c]._id == p.choice){
            console.log('afegint');
            poll.choices[c].votes.push(vote);
            callback(poll);
          }
        }
      }
    };

    Poll.findById(p.id, function(err, poll) {
        comprovarVot(poll, vote, function(pollComprovat){
          if (pollComprovat !== null ){
            afegirVot(pollComprovat, vote, p, function(pollFinal){
              pollFinal.save(function(error, doc){
                io.emit('poll', doc);
              });
            });
          }
          else {
            io.emit('error', 'No pots votar dos pics la mateixa enquesta');
          }
        });
		});
	});

	socket.on('disconnect', function() {
		// eliminam l'usuari de la llista de connectats
		usersOnline.splice(usersOnline.indexOf(socket.handshake.session.uid), 1);
		io.emit('users online', {
			'users_online': usersOnline
		});
		// afegim la darrera hora
		actualitzarHora(this);
	});
});

// start server
http.listen(3000, "0.0.0.0");


module.exports = app;
