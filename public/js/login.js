var domini = window.location.href;
domini = domini.split('/')[0];

function registrarUsuari(){
  var url = domini + '/api/users';
  var con = new XMLHttpRequest();

  //comprovar que els dos password coincideixen
  var user = document.getElementById('user').value;
  var password = document.getElementById('password').value;

  //fer post al servidor amb l'usuari i la password
  var parametres = 'nom=' + user + '&contrasenya=' + password;
  con.open("post", url, true);
  con.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  con.onreadystatechange = function(){
    if (con.readyState == 4) {
      var j = JSON.parse(con.responseText);
      if (j.status == 'success') {
        setTimeout(function(){
          Materialize.toast('Logged', 2000);
          window.location = '/';
        }, 2000);
      }
    }
  };
  con.send(parametres);
}

function loginUsuari(){
  var url = domini + '/api/users/login';
  var con = new XMLHttpRequest();

  //comprovar que els dos password coincideixen
  var user = document.getElementById('user').value;
  var password = document.getElementById('password').value;

  //fer post al servidor amb l'usuari i la password
  var parametres = 'nom=' + user + '&contrasenya=' + password;
  con.open("post", url, true);
  con.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  con.onreadystatechange = function(){
    if (con.readyState == 4) {
      var j = JSON.parse(con.responseText);
      if (j.status == 'success') {
          window.location = '/';
      }
      else {
        Materialize.toast(j.data.error, 2000);
      }
    }
  };
  con.send(parametres);
}

window.onload = function (){
  var registrar = document.getElementById('register');
  if (registrar !== null){
    registrar.onclick = function(){
      registrarUsuari();
    };
  }
  var login = document.getElementById('login');
  if (login !== null){
    login.onclick = function(){
      loginUsuari();
    };
  }
};
