var dades;
var missatgesGlobal = [];
var usuaris = [];
var nusuaris_online = [];
var numopcions = 2;
var domini = window.location.href;
domini = domini.split('/')[0];

function enviarMissatge() {
	console.log('enviant');
	var url = domini + '/api/missatges';
	var con = new XMLHttpRequest();
	var cont = document.getElementById('textArea').value;
	//var cont = 'tranquiloko';
	var parametres = 'contingut=' + cont;
	//console.log(parametres);
	con.open("post", url, true);
	con.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	con.onreadystatechange = function() {
		if (con.readyState == 4) {
			//console.log('sent');
			cont.value = '';
		}
	};
	con.send(parametres);
}

function getMissatges() {
	var dades = new XMLHttpRequest();
	var url = domini + '/api/missatges';
	var missatges = document.getElementById('missatges');
	dades.onreadystatechange = function() {
		if (dades.readyState == 4) {
			var j = JSON.parse(dades.response);
			j.reverse();
			for (var variable in j) {
				getUsuari(j[variable]);
			}
		}
	};
	dades.open("GET", url, true);
	dades.send(null);
}

function getPolls(){
  var dades = new XMLHttpRequest();
  var url = domini + '/api/polls';
  var polls = document.getElementById('missatges');
  dades.onreadystatechange = function() {
    if (dades.readyState == 4) {
      var j = JSON.parse(dades.response);
      j = j.data;
      for (var variable in j) {
        paintPoll(j[variable]);
      }
    }
  };
  dades.open("GET", url, true);
  dades.send(null);
}

function usuarisOnline() {
	var convertirNom = function(id, callback) {
		usuaris.some(function(el) {
			//console.log(missatge);
			if (el._id == id) {
				callback(el.nom);
			}
		});
	};
	for (var u in usuaris_online) {
		convertirNom(usuaris_online[u], function(nom) {
			if (nusuaris_online.indexOf(nom) == -1) {
				nusuaris_online.push(nom);
			}
		});
	}
}

function netejarUsuaris() {
	var usuaris = document.getElementById('usuaris');
	while (usuaris.lastChild) {
		usuaris.lastChild.remove();
	}
}

function pintarUsuari() {
	netejarUsuaris();
	var usuaris = document.getElementById('usuaris');
	var header = document.createElement('li');
	header.className = 'collection-header';
	header.appendChild(document.createTextNode('Usuaris'));
	usuaris.appendChild(header);
	var pintar = function(usuari) {
		/*var icon = document.createElement('i');
		icon.appendChild(document.createTextNode('person'));
		icon.className = 'material-icons tiny';*/
		var li = document.createElement('li');
		//li.appendChild(icon);
		li.className = 'collection-header';
		var span = document.createElement('span');
		span.className = 'title';
		span.appendChild(document.createTextNode(usuari));
		li.appendChild(span);
		usuaris.appendChild(li);
		console.log(li);
	};
	for (var u in nusuaris_online) {
		pintar(nusuaris_online[u]);
	}
}


function getUsuari(m) {
	var hiEs = function(missatge, callback) {
		usuaris.some(function(el) {
			//console.log(missatge);
			if (el._id == missatge.emisor) {
				callback(el.nom);
			}
		});
	};

	var gg = function(missatge, usuari, callback) {
		missatge.enom = usuari;
		callback(missatge);
	};

	hiEs(m, function(usuari) {
		gg(m, usuari, function(final) {
			pintarMissatge(final);
		});
	});
}

function pintarMissatge(context) {
	//var source = document.getElementById('missatge-template').innerHTML;

	var missatges = document.getElementById('missatges');
	missatgesGlobal.push(context);
  missatges.className = 'col s12 l12';
	var dCon = new Date(darrera_connexio);
	var dMiss = new Date(context.hora);
	var li;
	var nou = function() {
		li = document.createElement('li');
		var a = document.createElement('a');
		li.appendChild(a);
		a.appendChild(document.createTextNode(context.enom));

		p.appendChild(document.createTextNode(context.contingut));

		li.setAttribute('eid', context.emisor);
		li.setAttribute('class', 'collection-item');
		li.appendChild(p);
		missatges.appendChild(li);
	};
	var p = document.createElement('p');
	var hora = document.createElement('span');
	hora.className = 'hora';
	var horaObj = new Date(context.hora);
  var h = horaObj.getHours() < 10 ? '0' + horaObj.getHours() : horaObj.getHours();
  var m = horaObj.getMinutes() < 10 ? '0' + horaObj.getMinutes() : horaObj.getMinutes();
	var horaTxt = h + ':' + m ;
  //p.className = 'flow-text';
  hora.className = 'hora right';
	hora.appendChild(document.createTextNode(horaTxt));
	p.appendChild(hora);
	if (missatges.hasChildNodes()) {
		if (missatges.lastChild.getAttribute('eid') === context.emisor) {
			li = missatges.lastChild;
			p.appendChild(document.createTextNode(context.contingut));
			li.appendChild(p);
		} else {
			nou();
		}
	} else {
		nou();
	}
	if (dCon - dMiss < 0 && me != context.enom) {
		var badge = document.createElement('span');
		badge.className = 'new badge';
		p.appendChild(badge);
	}

	missatges.scrollTop = missatges.scrollHeight;
	/*if (missatges.lastChild.getAttribute('hora') > context.hora){
	  missatges.appendChild(li);
	}
	else{
	  missatges.insertBefore(li, missatges.lastChild);
	}*/
}

function getDarreraConnexio() {
	var dades = new XMLHttpRequest();
	var url = domini + '/api/users/lastconnection/' + uid;
	dades.onreadystatechange = function() {
		if (dades.readyState == 4) {
			var j = JSON.parse(dades.response);
			darrera_connexio = j.darrera_connexio;
			getMissatges();
		}
	};
	dades.open("GET", url, true);
	dades.send(null);
}

function getUsuaris() {
	var dades = new XMLHttpRequest();
	var url = domini + '/api/users';
	var users = document.getElementById('users');
	dades.onreadystatechange = function() {
		if (dades.readyState == 4) {
			var j = JSON.parse(dades.response);
			if (j.status === 'success') {
				for (var variable in j.data) {
					usuaris.push(j.data[variable]);
				}
			}
		}
	};
	dades.open("GET", url, true);
	dades.send(null);
}

function cleanNew() {
	var badges = document.getElementsByClassName('new badge');
	console.log(badges);
	while (badges[0]) {
		badges[0].parentNode.removeChild(badges[0]);
	}
}

function createPoll(callback) {
  var poll_opcions = document.getElementById('poll-opcions').childNodes;
  var opcions = [];
  for (var opt in poll_opcions) {
    if (typeof poll_opcions[opt].value !== 'undefined'){
      opcions.push(poll_opcions[opt].value);
    }
  }
  var title = document.getElementById('poll-title').value;
  var obj = {title: title, choices: opcions};
  console.log(obj);
  callback(obj);
}

function paintPoll(obj) {
  var mostVoted, iVoted;

  var translateUsuari = function(id, callback) {
    usuaris.some(function(el) {
      if (el._id === id) {
        callback(el.nom);
      }
    });
  };

  var title = document.createElement('h5');
  var div = document.createElement('div');
  var user = document.createElement('a');
  translateUsuari(obj.user, function(text){
    console.log(text);
    user.appendChild(document.createTextNode(text));
  });

  div.appendChild(user);
  title.appendChild(document.createTextNode(obj.title));
  var poll = document.getElementById('polls');
  div.appendChild(title);
  div.className = 'poll';
  div.id = obj._id;
  poll.appendChild(div);
  var votar = function(id, name) {
    var poll = {};
    poll.id = name;
    poll.choice = id;
    console.log(poll);
    var socket = io();
    socket.emit('vote', poll);
  };

  var createOption = function(opt, name, iv, mv){
    var input = document.createElement('input');
    var label = document.createElement('label');
    var p = document.createElement('p');
    var span = document.createElement('span');
    span.className = 'right';
    var num_votes = document.createTextNode(opt.votes.length + ' vots');
    label.appendChild(document.createTextNode(opt.text));
    label.htmlFor = opt._id;
    input.id = opt._id;
    if (mv == opt.votes.length){
      p.style.fontWeight = 'bold';
    }

    if (opt._id === iv){
      console.log('wp');
      input.checked = true;
    }
    input.type = 'radio';
    input.className = 'with-gap';
    input.value = opt._id;
    input.name = name;
    input.onclick = function(){
      votar(this.id, this.name);
    };
    span.appendChild(num_votes);
    p.appendChild(input);
    p.appendChild(label);
    p.appendChild(span);
    div.appendChild(p);
  };

  var getWhatIVoted = function (obj, callback) {
    var mostVoted = 0;
    for (var c in obj.choices) {
      for (var v in obj.choices[c].votes){
        if (obj.choices[c].votes.length > mostVoted) {
          mostVoted = obj.choices[c].votes.length;
        }
        if (obj.choices[c].votes[v].uid === uid){
          iVoted = obj.choices[c]._id;
        }
      }
    }
    callback(iVoted, mostVoted);
  };

  getWhatIVoted(obj, function(iv, mv){
    for (var c in obj.choices){
      createOption(obj.choices[c], obj._id, iv, mv);
    }
  });
}

function toggleView(){
  var missatges = document.getElementById('colMissatge');
  var polls = document.getElementById('polls');
  var toggleLink = $('#toggleLink');
  if (toggleLink.html() == 'speaker_notes'){
    toggleLink.html('chat');
  }
  else {
    toggleLink.html('speaker_notes');
  }
  var transicio = function(id, id2){
    $(id).addClass('hide');
    $(id2).removeClass('hide');
  };
  if($('#polls').hasClass('hide')){
    transicio('#colMissatge', '#polls');

  }
  else {
    transicio('#polls', '#colMissatge');
  }
}




window.onload = function() {
	var textArea = document.getElementById('textArea');
	getDarreraConnexio();
	getUsuaris();
  getPolls();

	// Enquestes
	$('#new-message-button').click(function(e) {
		if ($('#new-message-button').children().first().text() == 'clear') {
			$('#footer').addClass('hide');
			$('#new-message-button').children().first().text('mode_edit');
		} else {
			$('#footer').removeClass('hide');
			$('#new-message-button').children().first().text('clear');
		}
	});

	$('#new-poll-button').click(function(e) {
		$('#modal-poll').openModal();
	});

	$('#add-option-modal').click(function(e) {
		$('<input/>').attr({
			type: 'text',
			name: 'modal-option-' + numopcions,
			placeholder: 'Opció ' + numopcions,
			autofocus: 'true'
		}).appendTo('#poll-opcions');
		numopcions++;
	});

	// enviar poll
  $('#poll-button').click(function(e) {
		createPoll(function(poll){
      socket.emit('poll', poll);
    });
	});

	var socket = io();
	// enviar
	$('#textArea').keypress(function(e) {
		if (e.which == 13) {
			socket.emit('chat message', $('#textArea').val());
			//enviarMissatge();
			$('#textArea').val('');
		}
	});

	// rebre
	socket.on('chat message', function(msg) {
		var missatge = msg.missatge;
		missatge.enom = msg.nom;
		console.log(msg);
		darrera_connexio = msg.darrera_connexio;
		pintarMissatge(missatge);
		console.log(msg.users_online);
		cleanNew();
	});

	socket.on('users online', function(obj) {
		usuaris_online = obj.users_online;
    // ho ficam en un timeout per corregir la diferència entre la petició ajax
    // i la del socket.io
    setTimeout(usuarisOnline, 100);
		setTimeout(pintarUsuari, 100);
	});

  socket.on('poll', function(obj){
    $('#'+obj._id).remove();
    paintPoll(obj);
    Materialize.toast('Hi ha una nova enquesta', 4000);
  });

  socket.on('error', function(obj){
     Materialize.toast(obj, 4000);
  });

};

$(window).resize(function() {
	$('#colMissatge').height(($(window).height() - 200) > 200 ? $(window).height() - 200 : 200);
	$('#missatges').height(($(window).height() - 200) > 200 ? $(window).height() - 200 : 200);
  $('#polls').height(($(window).height() - 200) > 200 ? $(window).height() - 200 : 200);
});
