var express = require('express');
var router = express.Router();
var User = require('../models/user');

router.route('/users')
	// Registre
	.post(function(req, res) {
		var user = new User();
		user.nom = req.body.nom;
		user.contrasenya = req.body.contrasenya;
		req.session.nom = req.body.nom;
		req.session.uid = user.id;
		user.save(function(err) {
			if (err) res.send(err);
			res.send({
				'status': 'success',
				'data': {
					'nom': req.body.nom
				}
			});

		});
	})

// Rebre un usuari sense contrasenya
.get(function(req, res) {
	User.find(function(err, user) {
		var userWithoutPassword = [];
		if (err) res.send(err);
		for (var variable in user) {
			userWithoutPassword.push({
				'_id': user[variable]._id,
				'nom': user[variable].nom,
				'darrera_connexio': user[variable].darrera_connexio
			});
		}
		res.send({
			'status': 'success',
			'data': userWithoutPassword
		});
	});
});

router.route('/users/login/')
	// Autentificacio
	.post(function(req, res) { // GET por Id
		console.log(req.body);
		var fail = function() {
			res.json({
				'status': 'fail',
				'data': {
					'error': 'l\'usuari i la contrasenya no coincideixen'
				}
			});
		};
		// comprovam si s'ha passat el paràmetre nom
		if (typeof req.body.contrasenya !== 'undefined' && typeof req.body.nom !== 'undefined') {
			// agafam l'objecte user de la bbdd
			User.findOne({
				nom: req.body.nom
			}, function(err, user) {
				if (err) res.send(err);
				// si l'usuari no hi és ens torna un null.
				if (user !== null) {
					// comprovam que les contrasenyes coincideixen
					if (req.body.contrasenya === user.contrasenya) {
						req.session.nom = user.nom;
						req.session.uid = user.id;
						req.session.darrera_connexio = user.darrera_connexio;
						res.json({
							'status': 'success',
							'data': {
								'nom': req.session.nom
							}
						});
					} else {
						fail();
					}
				} else {
					fail();
				}
			});
		} else {
			fail();
		}
	});

//Last connection
router.route('/users/lastconnection/:id')
	.get(function(req, res) { // GET por Id
		User.findById(req.params.id, function(err, user) {
			if (err) res.send(err);
			else {
				res.json({
					'darrera_connexio': user.darrera_connexio
				});
			}
		});
	});

/*
router.route('/users/:id')
  .get(function(req, res) { // GET por Id
    User.findById(req.params.id, function(err, user) {
      if (err) res.send(err);
      else {
        res.json(user);
      }
    });
  })
  // actualizar User con Id
  .put(function(req, res) {
    User.findById(req.params.id, function(err, user) {
      if (err) res.send(err);
      user.nom = req.body.nom;
      user.contrasenya = req.body.contrasenya;

      // Guardamos la User
      user.save(function(err) {
        if (err) res.send(err);
        res.json({
          message: 'Usuari actualizada!'
        });
      });
    });
  })
  // eliminar  con Id
  .delete(function(req, res) {
    User.remove({
      _id: req.params.id
    }, function(err, user) {
      if (err) res.send(err);
      res.json({
        message: 'Usuari eliminada con exito'
      });
    });
  });
*/

module.exports = router;
