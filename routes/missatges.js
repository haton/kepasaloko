var express = require('express');
var router = express.Router();
var Missatge = require('../models/missatge');

var not_authorized = function(res){
  res.json({
    'status' : 'fail',
    'data' : {
      'error' : 'not authorized'
    }
  });
};

var content_error = function(res){
  res.json({
    'status' : 'fail',
    'data' : {
      'error' : 'content error'
    }
  });
};

router.route('/missatges')
// rebre tots els missatges
.get(function(req, res){
  // comprovam que tengui la sessió iniciada
  if (typeof req.session.uid !== 'undefined'){
    Missatge.find(function(err, missatge) {
      if (err) res.send(err);
      res.json(missatge);
    }).sort({$natural:-1}).limit(50);
  }
  else {
    not_authorized(res);
  }
});

// Afegir missatges per post
/*.post(function(req, res){
  // afegir missatges
	var missatge = new Missatge();
	missatge.contingut = req.body.contingut;
  //comprovam que tengui la sessío iniciada
  if (typeof req.session.uid !== 'undefined'){
    if (missatge.contingut !== '') {
      missatge.emisor = req.session.uid;
      missatge.save(function(err){
        if(err) res.send(err);
        res.json({
          'status' : 'succsess',
          'data' : missatge
        });
      });
    }
    else {
      content_error(res);
    }
  }
  else {
    not_authorized(res);
  }
})*/

// Rutas para Missatge específica
/*
router.route('/missatges/:id')
.get(function(req, res) { // GET por Id
    Missatge.findById(req.params.id, function(err, missatge) {
        if (err)res.send(err);
        res.json(missatge);
    });
})
// actualizar Missatge con Id
.put(function(req, res) {
    Missatge.findById(req.params.id, function(err, missatge) {
        if (err) res.send(err);
        missatge.nom = req.body.nom;
	      missatge.contrasenya = req.body.contrasenya;

        // Guardamos la Missatge
        missatge.save(function(err) {
            if (err) res.send(err);
            res.json({message:'Usuari actualizada!'});
        });
    });
})
// eliminar  con Id
.delete(function(req, res) {
    Missatge.remove({
        _id: req.params.id
    }, function(err, missatge) {
        if (err) res.send(err);
        res.json({message:'Usuari eliminada con exito'});
    });
});
*/

module.exports = router;
