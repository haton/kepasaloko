var express = require('express');
var router = express.Router();

// pàgina d'iniciada
// passa el nom d'usuari i l'identificador d'usuari
router.get('/', function (req, res) {
    if (typeof req.session.nom === 'undefined'){
      res.redirect('/login');
    }
    else {
      res.render('home', {
        title : 'kepasaloko',
        nom : req.session.nom,
        uid : req.session.uid
      });
    }
});

// pàgina de login
router.get('/login', function (req, res) {
  if (typeof req.session.nom === 'undefined'){
    res.render('login', {
      title : 'login',
      layout : 'login'
    });
  }
  else {
    res.redirect('/');
  }
});

// pàgina de registre
router.get('/register', function (req, res) {
  if (typeof req.session.nom === 'undefined'){
    res.render('register', {
      title : 'login',
      layout : 'login'
    });
  }
  else {
    res.redirect('/');
  }
});

// pàgina de sortida
router.get('/logout', function (req, res) {
  req.session.destroy(function(){
    res.redirect('/login');
  });
});

module.exports = router;
