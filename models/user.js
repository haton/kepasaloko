var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var UserSchema   = new Schema({
  nom: String,
	contrasenya: String,
  darrera_connexio: { type: Date, default: Date.now }
});

module.exports = mongoose.model('User', UserSchema);
