var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
var User         = mongoose.model('User');

var MissatgeSchema   = new Schema({
  contingut: String,
	emisor: { type: Schema.ObjectId, ref: "User" },
  hora: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Missatge', MissatgeSchema);
