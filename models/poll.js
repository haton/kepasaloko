var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
var User         = mongoose.model('User');

var VoteSchema = new Schema({
  uid: { type: Schema.ObjectId, ref: "User" }
});

var ChoiceSchema = new Schema({
  text: String,
  votes: [VoteSchema]
});

var PollSchema   = new Schema({
  title: {type: String, required: true},
  choices : [ChoiceSchema],
  user : { type: Schema.ObjectId, ref: "User" }
});

module.exports = {poll : mongoose.model('Poll', PollSchema), choice : mongoose.model('Choice', ChoiceSchema), vote : mongoose.model('Vote', VoteSchema)};
